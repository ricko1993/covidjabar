from flask import Flask, render_template
import requests, schedule

app = Flask (__name__)

def jabar():
    api_url = "https://api.kawalcorona.com/indonesia/provinsi"
    rstl= requests.get(api_url).json()
    cek=rstl[1]
    ambil_dic= cek ["attributes"]
    a=[ambil_dic]
    return a

realtime_jabar = schedule.every(60).seconds.do(jabar)

data_jabar = jabar()

@app.route("/")
def index():
    #return render_template('TESTLAGI.HTML')
    return render_template('INDEX2.HTML', data_jabar=data_jabar)

if __name__=="__main__":
    app.run(debug=True)